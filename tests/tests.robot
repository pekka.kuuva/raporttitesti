*** Settings ***
Library     SeleniumLibrary

*** Test Cases ***
Test Firefox
    Open Browser   http://35.205.103.91/    Firefox
    Title Should Be     Hello World
    Close Browser

Test Chrome
    ${chrome_options}=    Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    Call Method    ${chrome_options}    add_argument    --no-sandbox
    Create Webdriver    Chrome      chrome_options=${chrome_options}
    Go To   http://35.205.103.91/
    Title Should Be     Hello World
    Close Browser
